package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner (StudentRepository repository){
        return args -> {
            Student masud = new Student(
                    "Masud",
                    "masud.aiub@gmail.com",
                    LocalDate.of(1994, Month.JULY, 1)
            );

            Student mike = new Student(
                    "Mike",
                    "mike.aiub@gmail.com",
                    LocalDate.of(1996, Month.JULY, 1)
            );

            repository.saveAll(
                    List.of(masud, mike)
            );
        };
    }
}
